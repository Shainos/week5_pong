#include "Application.h"

Application::Application()
{
	mWindow.create(sf::VideoMode(1200, 800), "Pong Time");
	mGame.Init(mWindow);
}
Application::~Application()
{

}
void Application::Run()
{
	// Start the game loop 
	//sf::Clock clock;
	while (mWindow.isOpen())
	{
		// Process events
		sf::Event event;
		while (mWindow.pollEvent(event))
		{
			// Close window: exit
			if (event.type == sf::Event::Closed)
				mWindow.close();
	
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
			mWindow.close();

		//sElapsedSecs = clock.restart();

		// Clear screen
		mWindow.clear();

		mGame.Update();
		mGame.Render();

		// Update the window
		mWindow.display();
	}
}
