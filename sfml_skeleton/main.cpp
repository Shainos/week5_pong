#include <assert.h>
#include <ctime>
#include "SFML/Graphics.hpp"

#include "GameConsts.h"
#include "Application.h"
using namespace std;


int main()
{

	Application app;
	app.Run();

	return EXIT_SUCCESS;

	// Create the main window
	sf::RenderWindow window(sf::VideoMode(GC::SCREEN_RES.x, GC::SCREEN_RES.y), "Bing Bong Ping Pong!");
	
	//bats and ball
	sf::Texture texPdl;
	if (!texPdl.loadFromFile("data/pongSprites.png"))
		assert(false);
	texPdl.setSmooth(true);

	//instantiating ball to display
	sf::Sprite sprBall(texPdl, sf::IntRect(115, 15, 20, 20));
	sprBall.setPosition(GC::SCREEN_RES.x / 2.f, GC::SCREEN_RES.y / 2.f);
	sprBall.setOrigin(10,10);
	sprBall.setScale(3, 3);

	//player 1 
	sf::Sprite sprBat1(texPdl, sf::IntRect(50, 2, 50, 150));
	sprBat1.setPosition(GC::SCREEN_RES.x / 10.f, GC::SCREEN_RES.y / 2.f);
	sprBat1.setOrigin(sprBat1.getTextureRect().width / 2.f, sprBat1.getTextureRect().height / 2.f);

	//player 2 
	sf::Sprite sprBat2(texPdl, sf::IntRect(0, 2, 50, 150));
	sprBat2.setPosition(GC::SCREEN_RES.x*(9.f /10.f), GC::SCREEN_RES.y / 2.f);
	sprBat2.setOrigin(sprBat2.getTextureRect().width / 2.f, sprBat2.getTextureRect().height / 2.f);

	
	// Start the game loop 
	while (window.isOpen())
	{
		// Process events
		sf::Event event;
		while (window.pollEvent(event))
		{
			// Close window: exit
			if (event.type == sf::Event::Closed) 
				window.close();
			if (event.type == sf::Event::TextEntered)
			{
				if (event.text.unicode == GC::ESCAPE_KEY)
					window.close();
			}
		} 
				
		// Clear screen
		window.clear();

		//drawing to screen
		window.draw(sprBall);
		window.draw(sprBat1);
		window.draw(sprBat2);
		// Update the window
		window.display();
	}

	return EXIT_SUCCESS;
}

