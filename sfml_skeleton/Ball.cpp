#include "Ball.h"

Ball::Ball()
{

}
Ball::~Ball()
{

}
void Ball::Init(const sf::Texture& tex, Vec2f pos, sf::IntRect texPos, Vec2f origin, float rotation)
{
	//sets relavant data during initialising
	objSprite.setTexture(tex);
	objSprite.setTextureRect(texPos);
	objSprite.setOrigin(origin.x, origin.y);
	objSprite.setPosition(pos.x, pos.y);
	objSprite.setRotation(rotation);
	SetVel(Vec2f{ cos(Deg2Rad(rotation)) * GC::BALL_SPEED, sin(Deg2Rad(rotation)) * GC::BALL_SPEED });
}
void Ball::SetVel(Vec2f nVel)
{
	//sets new velocity
	vel = nVel;
}
Vec2f Ball::GetVel()
{
	//gets current velocity
	return vel;
}
void Ball::Update(float inc)
{
	//moves object
	objSprite.move(vel.x*inc, vel.y*inc);
}
void Ball::ResetBall(float newRot)
{
	//sets a new direction for the ball to go in when game is played
	SetVel(Vec2f{ cos(Deg2Rad(newRot)) * GC::BALL_SPEED, sin(Deg2Rad(newRot)) * GC::BALL_SPEED });
	//resets the acceleration so the ball is at its default speed
	Acceleration = 0;
}