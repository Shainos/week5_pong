#include "Score.h"

Score::Score()
{



}
Score::~Score()
{

}
void Score::Init(const sf::Font& t, Vec2f pos, int fontSize, std::string str)
{
	//sets relavant data during initialising
	txt.setFont(t);
	txt.setCharacterSize(fontSize);
	txt.setPosition(pos.x, pos.y);
	txt.setString(str);
}
void Score::Render(sf::RenderWindow& window)
{
	//draw score to the screeen
	window.draw(txt);
}
void Score::ChangeText(int p1Score, int p2Score)
{
	//sets updates the score at the top of the screen
	txt.setString(std::to_string(p1Score) + " : " + std::to_string(p2Score));
}
