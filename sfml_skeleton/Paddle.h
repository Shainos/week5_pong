#ifndef PADDLE_H
#define PADDLE_H

#include "GameObject.h"
#include "Ball.h"

class Paddle : public GameObject
{
public:
	Paddle();
	~Paddle();
	void CheckCollision(Ball& ball);
private:
	bool Colliding = false;
};

#endif // !PADDLE_H