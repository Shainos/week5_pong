#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include "SFML/Graphics.hpp"
#include "GameConsts.h"
#include <string>
class GameObject
{
public:
	void SetPos(Vec2f newpos);
	Vec2f GetPos();
	void Init(const sf::Texture& tex, Vec2f pos, sf::IntRect texPos, Vec2f origin);
	void Init(const sf::Font& t, Vec2f pos, int fontSize, std::string);
	virtual void Render(sf::RenderWindow& window);
	virtual void Update();
	virtual void Update(float increment);
	void Update(Vec2f increment);
protected:
	sf::Sprite objSprite;
private:
	

};

#endif // !GAMEOBJECT_H