#ifndef GAMECONSTS_H
#define GAMECONSTS_H



#include <string>

//dimensions in 2D that are whole numbers
struct Vec2i
{
	int x, y;
};

//dimensions in 2D that are floating point numbers
struct Vec2f
{
	float x, y;
};

 
/*
A box to put Games Constants in.
These are special numbers with important meanings (screen width, 
ascii code for the escape key, number of lives a player starts with,
the name of the title screen music track, etc.
*/
namespace GC
{
	//game play related constants to tweak
	const Vec2i SCREEN_RES{ 1200,800 };
	const float PADDLE_SPEED{600.f};
	const float PADDLE_WIDTH{60.f};
	const float BALL_SPEED{ 400.f };
	const float BALL_RADIUS{10.f};
	const char ESCAPE_KEY{ 27 };

}


const float PI = 3.14159265358979323846f;

inline float Deg2Rad(float deg) {
	return deg * (PI / 180.f);
}


#endif // !GAMECONSTS_H
