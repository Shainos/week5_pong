#ifndef GAME_H
#define GAME_H

#include "SFML/Graphics.hpp"
#include "GameConsts.h"
#include "GameObject.h"
#include "Ball.h"
#include "Paddle.h"
#include "Score.h"
class Game {
public:
	Game();
	~Game();	
	enum class MyState {
		INITIALISING,
		PLAYING,
		WAITING_P1,
		WAITING_P2
	};
	void Init(sf::RenderWindow&);
	void Render();
	void Update();
	void Reset(MyState);
	
	
private:
	sf::RenderWindow *pWindow;
	sf::Time Timer;	//track how much time each update/render takes for smooth motion
	sf::Clock clock;
	sf::Texture pongSprites;
	sf::Font pongFont;
	MyState mState;
	int p1Score = 0;
	int p2Score = 0;
	Ball mBall;
	Paddle mPaddle1;
	Paddle mPaddle2;
	Score mPrompt;
	Score mScore;
};

#endif // !GAME_H