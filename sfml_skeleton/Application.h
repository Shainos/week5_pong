#ifndef APPLICATION_H
#define APPLICATION_H

#include "SFML/Graphics.hpp"
#include "Game.h"
class Application {
public:
	Application();
	~Application();
	void Run();
private:
	Game mGame;
	sf::RenderWindow mWindow;		//for rendering
};

#endif // !APPLICATION_H