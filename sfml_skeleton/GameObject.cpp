#include "GameObject.h"
#include <cmath>

void GameObject::SetPos(Vec2f nPos)
{
	//sets new position
	objSprite.setPosition(nPos.x, nPos.y);
}
Vec2f GameObject::GetPos()
{
	//gets the current position
	return Vec2f{ objSprite.getPosition().x,objSprite.getPosition().y };
}

void GameObject::Render(sf::RenderWindow& window)
{
	//draws object to the screen
	window.draw(objSprite);
}
void GameObject::Init(const sf::Texture& tex, Vec2f pos, sf::IntRect texPos, Vec2f origin)
{
	//sets relavant data during initialising
	objSprite.setTexture(tex);
	objSprite.setTextureRect(texPos);
	objSprite.setOrigin(origin.x, origin.y);
	objSprite.setPosition(pos.x, pos.y);

}

void GameObject::Init(const sf::Font& t, Vec2f pos, int fontSize, std::string)
{
	
}
void GameObject::Update()
{

}
void GameObject::Update(float)
{

}
void GameObject::Update(Vec2f inc)
{
	//moves object
	objSprite.move(inc.x, inc.y);
}




