#ifndef SCORE_H
#define SCORE_H

#include "GameObject.h"
class Score : public GameObject
{
public:
	Score();
	~Score();
	void Init(const sf::Font& t, Vec2f pos, int fontSize, std::string);
	void Render(sf::RenderWindow&);
	void ChangeText(int p1Score, int p2Score);
private:
	std::string mssg;
	sf::Text txt;
	sf::Font fnt;
};

#endif // !SCORE_H