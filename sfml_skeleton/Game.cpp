#include "Game.h"
#include <cassert>
#include <cstdlib>
Game::Game()
	:mState(MyState::INITIALISING)
{
	srand((unsigned int)time(nullptr)*1000);
}
Game::~Game()
{

}
void Game::Init(sf::RenderWindow& mWin)
{
	//creating a local refernce to the window
	pWindow = &mWin;

	//loading texture
	if (!pongSprites.loadFromFile("data/pongSprites.png"))
		assert(false);
	//loading font
	if (!pongFont.loadFromFile("data/AirStream.ttf"))
		assert(false);
	
	//initialising objects
	mPaddle1.Init(pongSprites, Vec2f{ GC::SCREEN_RES.x *0.1f,GC::SCREEN_RES.y *0.5f }, sf::IntRect{ 5, 10, 40, 130 }, Vec2f{ 20, 65 });
	mPaddle2.Init(pongSprites, Vec2f{ GC::SCREEN_RES.x *0.9f,GC::SCREEN_RES.y *0.5f }, sf::IntRect{ 55, 10, 40, 130 }, Vec2f{ 20, 65 });
	mBall.Init(pongSprites, Vec2f{ GC::SCREEN_RES.x *0.5f,GC::SCREEN_RES.y *0.5f }, sf::IntRect{ 100, 0, 50, 50 }, Vec2f{ 25.f, 25.f }, (float)(rand()%359));
	mPrompt.Init(pongFont, Vec2f{ GC::SCREEN_RES.x *0.45f, GC::SCREEN_RES.y *0.9f }, 20,"Press Space to Start");
	mScore.Init(pongFont, Vec2f{ GC::SCREEN_RES.x *0.48f, GC::SCREEN_RES.y *0.1f }, 20,"0 : 0");
	
	//setting initial state
	mState = MyState::WAITING_P1;
}

void Game::Update()
{

	if ((mState == MyState::PLAYING) || (mState == MyState::WAITING_P1) || (mState == MyState::WAITING_P2))
	{	

		//moving player 1
		if ((sf::Keyboard::isKeyPressed(sf::Keyboard::W)) && (mPaddle1.GetPos().y >= GC::PADDLE_WIDTH))
		{
			mPaddle1.Update(Vec2f{ 0,-GC::PADDLE_SPEED * Timer.asSeconds() });
		}
		else if ((sf::Keyboard::isKeyPressed(sf::Keyboard::S)) && (mPaddle1.GetPos().y <= GC::SCREEN_RES.y - GC::PADDLE_WIDTH))
		{
			mPaddle1.Update(Vec2f{ 0,GC::PADDLE_SPEED * Timer.asSeconds() });
		}

		//moving player 2
		if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) && (mPaddle2.GetPos().y >= GC::PADDLE_WIDTH))
		{
			mPaddle2.Update(Vec2f{ 0,-GC::PADDLE_SPEED * Timer.asSeconds() });
		}
		else if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) && (mPaddle2.GetPos().y <= GC::SCREEN_RES.y - GC::PADDLE_WIDTH))
		{
			mPaddle2.Update(Vec2f{ 0,GC::PADDLE_SPEED * Timer.asSeconds() });
		}


		if (mState == MyState::PLAYING)
		{
			//checking ball collision with paddles
			mPaddle1.CheckCollision(mBall);
			mPaddle2.CheckCollision(mBall);
			
			//moving ball
			mBall.Update(Timer.asSeconds());
		}
		else if (mState == MyState::WAITING_P1)
		{
			//ball starting next to paddle
			mBall.SetPos(Vec2f{ mPaddle1.GetPos().x + GC::BALL_RADIUS + 12.f, mPaddle1.GetPos().y });
		}
		else if (mState == MyState::WAITING_P2)
		{
			//ball starting next to paddle
			mBall.SetPos(Vec2f{ mPaddle2.GetPos().x - GC::BALL_RADIUS - 12.f, mPaddle2.GetPos().y });
		}

	}
	
	//moving the ball after pressing space to start the game
	if (((mState == MyState::WAITING_P1) || (mState == MyState::WAITING_P2)) && (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)))
	{
		mState = MyState::PLAYING;
	}
	
	//ball bouncing of the top/bottom
	if (mBall.GetPos().y >= GC::SCREEN_RES.y - GC::BALL_RADIUS)
	{
		mBall.SetVel(Vec2f{ mBall.GetVel().x,-mBall.GetVel().y });
	}else if (mBall.GetPos().y <= GC::BALL_RADIUS)
	{
		mBall.SetVel(Vec2f{ mBall.GetVel().x,-mBall.GetVel().y });
	}
	
	//checking if the ball has reached the edges(left/right)
	if (mBall.GetPos().x >= GC::SCREEN_RES.x - GC::BALL_RADIUS)
	{
		p1Score += 1;
		Reset(MyState::WAITING_P2);
	}
	else if (mBall.GetPos().x <= GC::BALL_RADIUS)
	{		
		p2Score += 1;
		Reset(MyState::WAITING_P1);
	}


	mPrompt.Update();
	mScore.Update();

	Timer = clock.restart();
}
void Game::Render()
{
	//rendering all objects
	mPaddle1.Render(*pWindow);
	mPaddle2.Render(*pWindow);
	mBall.Render(*pWindow);
	//renders prompt to press space to start the game
	if ((mState == MyState::WAITING_P1) || (mState == MyState::WAITING_P2))
	{
		mPrompt.Render(*pWindow);
	}
	
	mScore.Render(*pWindow);
}

void Game::Reset( MyState newstate)
{
	//resetting the ball with a random direction
	mBall.ResetBall((float)(rand() % 359));
	//changes state depending on whther p1 or p2 won
	mState = newstate;
	//resetting paddle position
	mPaddle1.SetPos(Vec2f{ GC::SCREEN_RES.x *0.1f,GC::SCREEN_RES.y *0.5f });
	mPaddle2.SetPos(Vec2f{ GC::SCREEN_RES.x *0.9f,GC::SCREEN_RES.y *0.5f });
	//adjusts score being displayed
	mScore.ChangeText(p1Score, p2Score);
}