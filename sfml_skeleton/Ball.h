#ifndef BALL_H
#define BALL_H

#include "SFML/Graphics.hpp"
#include "GameObject.h"
#include "GameConsts.h"
class Ball : public GameObject
{
public:
	Ball();
	~Ball();
	void Update(float increment);
	void SetVel(Vec2f newVelocity);
	Vec2f GetVel();
	void ResetBall(float);
	void Init(const sf::Texture& tex, Vec2f pos, sf::IntRect texPos, Vec2f origin, float rotation);
	float Acceleration = 0;
private:
	Vec2f vel;

};
#endif // !BALL_H