#include "Paddle.h"

Paddle::Paddle()
{

}
Paddle::~Paddle()
{

}
void Paddle::CheckCollision(Ball& ball)
{
	//sets local ball vals to stop calling .GetPos multiple times
	float ballX = ball.GetPos().x;
	float ballY = ball.GetPos().y;

	//checks if the ball is overlapping with the paddle
	if ((ballX - GC::BALL_RADIUS <= objSprite.getPosition().x + 10) && (ballX + GC::BALL_RADIUS >= objSprite.getPosition().x - 10))
	{
		if (((ballY - GC::BALL_RADIUS <= objSprite.getPosition().y + GC::PADDLE_WIDTH) && (ballY + GC::BALL_RADIUS >= objSprite.getPosition().y - GC::PADDLE_WIDTH)))
		{
			//stops the ball from recurring collisions with the paddle
			if (Colliding != true)
			{
				ball.Acceleration += 30;
				if (objSprite.getPosition().x <= GC::SCREEN_RES.x / 2)
				{

					ball.SetVel(Vec2f{ cos(Deg2Rad((objSprite.getPosition().y - ballY) / GC::PADDLE_WIDTH * 60))  * (GC::BALL_SPEED + ball.Acceleration), -sin(Deg2Rad((objSprite.getPosition().y - ballY) / GC::PADDLE_WIDTH * 60)) * (GC::BALL_SPEED + ball.Acceleration) });
				}
				else
				{

					ball.SetVel(Vec2f{ -cos(Deg2Rad((objSprite.getPosition().y - ballY) / GC::PADDLE_WIDTH * 60))  * (GC::BALL_SPEED + ball.Acceleration), -sin(Deg2Rad((objSprite.getPosition().y - ballY) / GC::PADDLE_WIDTH * 60)) * (GC::BALL_SPEED + ball.Acceleration) });
				}
				Colliding = true;
			}

		}
		else
		{
			//allows the ball to collide with the paddle again after it has left the area of the paddle
			Colliding = false;
		}
	}
	else
	{
		Colliding = false;
	}

}